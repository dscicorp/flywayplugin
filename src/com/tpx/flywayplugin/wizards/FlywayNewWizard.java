package com.tpx.flywayplugin.wizards;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWizard;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

/**
 * This wizard is used to create a versioned flyway migration script.
 */

public class FlywayNewWizard extends Wizard implements INewWizard {

	private static final String MIGRATION_ROOT = "/war/WEB-INF/db/migration";
	private static final String SQL_EXTENSION = ".sql";
	private static final String USE_DASH = "USE DASH;\n";
	private static final String PROD_ONLY = "ProdOnly";
	private static final String VERSION_DESCRIPTION_SEPARATOR = "__";

	private FlywayNewWizardPage page;
	private ISelection selection;

	/**
	 * Constructor for com.tpx.FlywayNewWizard.
	 */
	public FlywayNewWizard() {
		super();
		setNeedsProgressMonitor(true);
	}

	/**
	 * Adding the page to the wizard.
	 */
	@Override
	public void addPages() {
		page = new FlywayNewWizardPage(selection);
		addPage(page);
	}

	/**
	 * We will accept the selection in the workbench to see if we can initialize from it.
	 *
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}

	/**
	 * This method is called when 'Finish' button is pressed in the wizard. We will create an operation and run it using wizard as execution context.
	 */
	@Override
	public boolean performFinish() {
		final String projectName = page.getProjectName();
		final String jiraTicket = page.getJiraTicket();
		final boolean addProdOnlyStoredProc = page.getAddProdOnlyStoredProc();

		IRunnableWithProgress op = new IRunnableWithProgress() {
			@Override
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					doFinish(projectName, jiraTicket, addProdOnlyStoredProc, monitor);
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * The worker method. It will find the container, create the file if missing or just replace its contents, and open the editor on the newly created file.
	 */

	private void doFinish(String projectName, String jiraTicket, boolean addProdOnlyStoredProc, IProgressMonitor monitor) throws CoreException {
		monitor.beginTask("Creating " + jiraTicket, 2);
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = root.getProject(projectName);
		IPath path = project.getFullPath();
		path = path.append(getMigrationFileName(jiraTicket));
		final IFile file = root.getFile(path);

		try {
			InputStream stream = openContentStream(addProdOnlyStoredProc, jiraTicket);
			if (file.exists()) {
				file.setContents(stream, true, true, monitor);
			} else {
				file.create(stream, true, monitor);
			}
			stream.close();
		} catch (IOException e) {
		}
		monitor.worked(1);
		monitor.setTaskName("Opening file for editing...");
		getShell().getDisplay().asyncExec(new Runnable() {
			@Override
			public void run() {
				IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				try {
					IDE.openEditor(activePage, file, true);
				} catch (PartInitException e) {
				}
			}
		});
		monitor.worked(1);
	}

	private String getMigrationFileName(String jiraTicket) {
		StringBuilder builder = new StringBuilder();
		Calendar cal = Calendar.getInstance();
		builder.append(MIGRATION_ROOT);
		builder.append("/" + cal.get(Calendar.YEAR));
		builder.append("/V" + getMigrationVersion(cal));
		builder.append(VERSION_DESCRIPTION_SEPARATOR);
		builder.append(jiraTicket);
		builder.append(SQL_EXTENSION);
		return builder.toString();
	}

	private String getMigrationVersion(Calendar cal) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		return formatter.format(cal.getTime());
	}

	private InputStream openContentStream(boolean addProdOnlyStoredProc, String jiraTicket) {
		StringBuilder builder = new StringBuilder();
		builder.append(USE_DASH);
		String storedProcName = PROD_ONLY + jiraTicket.replace("-", "");
		if (addProdOnlyStoredProc) {
			builder.append("DROP PROCEDURE IF EXISTS `");
			builder.append(storedProcName);
			builder.append("`;\n");
			builder.append("DELIMITER $$\n");
			builder.append("USE `DASH`$$\n");
			builder.append("CREATE DEFINER=`agent`@`%` PROCEDURE `");
			builder.append(storedProcName);
			builder.append("`()\n");
			builder.append("BEGIN\n\n");
			builder.append("SELECT `Value` INTO @Environment FROM META.Metadata WHERE `Key` = 'db.environment';\n\n");
			builder.append("IF @Environment LIKE 'prod-db%' THEN\n\n");
			builder.append("-- Your code goes here\n\n");
			builder.append("END IF;\n");
			builder.append("END$$\n");
			builder.append("DELIMITER ;\n");
			builder.append("CALL `DASH`.`");
			builder.append(storedProcName);
			builder.append("`();\n");
			builder.append("DROP PROCEDURE IF EXISTS `");
			builder.append(storedProcName);
			builder.append("`;\n");
		}
		return new ByteArrayInputStream(builder.toString().getBytes());
	}
}