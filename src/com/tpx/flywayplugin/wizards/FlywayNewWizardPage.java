package com.tpx.flywayplugin.wizards;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * The "New" wizard page allows setting the container for the new file as well as the file name. The page will only accept file name without the extension OR
 * with the extension that matches the expected one (sql).
 */

public class FlywayNewWizardPage extends WizardPage {

	private Combo projectSelector;
	private Text jiraTicketText;
	private Button prodOnlyCheckmark;

	/**
	 * Constructor for SampleNewWizardPage.
	 *
	 * @param pageName
	 */
	public FlywayNewWizardPage(ISelection selection) {
		super("wizardPage");
		setTitle("Flyway Migration Script");
		setDescription("This wizard creates a new Flyway migration script.");
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 2;
		layout.verticalSpacing = 9;

		Label label = new Label(container, SWT.NULL);
		label.setText("&Eclipse project: ");
		projectSelector = new Combo(container, SWT.DROP_DOWN);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		projectSelector.setLayoutData(gd);

		label = new Label(container, SWT.NULL);
		label.setText("&Jira ticket:");

		jiraTicketText = new Text(container, SWT.BORDER | SWT.SINGLE);
		jiraTicketText.setLayoutData(gd);
		jiraTicketText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		prodOnlyCheckmark = new Button(container, SWT.CHECK);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		prodOnlyCheckmark.setLayoutData(gd);
		prodOnlyCheckmark.setText("Add Production-only Stored Procedure");

		initialize();
		dialogChanged();
		setControl(container);
	}

	public boolean getAddProdOnlyStoredProc() {
		return prodOnlyCheckmark.getSelection();
	}

	public String getJiraTicket() {
		return jiraTicketText.getText();
	}

	public String getProjectName() {
		return projectSelector.getText();
	}

	/**
	 * Ensures that the Jira ticket number is well-formed
	 */
	private void dialogChanged() {
		String jiraTicket = getJiraTicket();

		if (jiraTicket == null || jiraTicket.isEmpty()) {
			updateStatus("You must provide a Jira ticket number, e.g. DDK-1000");
			return;
		}
		int hyphenIndex = jiraTicket.indexOf('-');
		if (hyphenIndex < 2 || hyphenIndex >= jiraTicket.length()) {
			updateStatus("The Jira ticket number should be the board abbreviation, a hyphen, then a number, e.g. DDK-1000");
			return;
		}
		String board = jiraTicket.substring(0, hyphenIndex);
		String number = jiraTicket.substring(hyphenIndex);
		if (board.length() < 2) {
			updateStatus("The board abbreviation should be at least 2 letters, e.g. AT/BT/DDK");
			return;
		}
		try {
			Integer.parseInt(number);
		} catch (NumberFormatException nfe) {
			updateStatus("The Jira ticket number must be numeric, e.g. DDK-1000");
			return;
		}
		updateStatus(null);
	}

	/**
	 * Sets the project list and the initial Jira Ticket
	 */
	private void initialize() {
		IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
		if (projects != null) {
			for (IProject project : projects) {
				try {
					projectSelector.add(project.getDescription().getName());
				} catch (CoreException e) {
					// Ignore
				}
			}
			projectSelector.select(0);
		}
		jiraTicketText.setText("DDK-1000");
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}

}
